package se.miun.dsv.jee;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.glassfish.embeddable.GlassFish;
import org.glassfish.embeddable.GlassFishException;
import org.glassfish.embeddable.GlassFishProperties;
import org.glassfish.embeddable.GlassFishRuntime;

public class Bootstrap

{
	private static final int HTTP_PORT = 10000;

	public static void main(String[] args) throws IOException {
		try {
			GlassFishRuntime runtime = GlassFishRuntime.bootstrap();
			GlassFish glassfish = initializeAndStartGlassfishServer(runtime);
			AsAdmin asadmin = new AsAdmin(glassfish.getCommandRunner());

			String dbIp = args[0];
			String deployable = args[1];

			createDatabase(asadmin, dbIp);
			String appName = glassfish.getDeployer().deploy(new File(deployable));
			if (appName == null)
				Logger.getLogger(Bootstrap.class.getName()).log(Level.WARNING,
						"App could not be deployed. Is the name/path correct?");
		}

		catch (GlassFishException ex) {
			Logger.getLogger(Bootstrap.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private static void createDatabase(AsAdmin asadmin, String dbIp) {
		GlassfishServerDBConfig config = new GlassfishServerDBConfig("jee", "jee_user", "jee_password", dbIp);

		asadmin.createPostgresJdbcConnectionPool(config);
		asadmin.createJdbcResource(config);
	}

	private static GlassFish initializeAndStartGlassfishServer(GlassFishRuntime runtime) throws GlassFishException {
		GlassFishProperties glassfishProperties = new GlassFishProperties();
		glassfishProperties.setPort("http-listener", HTTP_PORT);
		GlassFish glassfish = runtime.newGlassFish(glassfishProperties);
		glassfish.start();
		return glassfish;
	}
}