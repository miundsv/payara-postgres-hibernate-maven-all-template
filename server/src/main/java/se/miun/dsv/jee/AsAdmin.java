package se.miun.dsv.jee;

import org.glassfish.embeddable.CommandResult;
import org.glassfish.embeddable.CommandResult.ExitStatus;
import org.glassfish.embeddable.CommandRunner;

import com.hazelcast.logging.Logger;

public class AsAdmin {

	private final CommandRunner commandRunner;

	public AsAdmin(CommandRunner commandRunner) {
		this.commandRunner = commandRunner;
	}

	/**
	 * assumes the default port 5432
	 */
	public void createPostgresJdbcConnectionPool(GlassfishServerDBConfig config) {

		String poolProperties = "User=" + config.getUserName() + ":Password=" + config.getPassword()
				+ ":URL=jdbc\\:postgresql\\://" + config.getIP() + "/" + config.getConnectionPoolId();

		run("create-jdbc-connection-pool", "--restype", "javax.sql.ConnectionPoolDataSource", "--datasourceclassname",
				"org.postgresql.ds.PGSimpleDataSource", "--property", poolProperties, config.getConnectionPoolId());

	}

	public void createJdbcResource(GlassfishServerDBConfig config) {
		run("create-jdbc-resource", "--connectionpoolid", config.getConnectionPoolId(), config.getJDBCResourceId());
	}

	private CommandResult run(String command, String... args) {
		CommandResult result = commandRunner.run(command, args);
		Logger.getLogger(AsAdmin.class).info(result.getOutput());
		if (result.getExitStatus().compareTo(ExitStatus.FAILURE) == 0)
			Logger.getLogger(AsAdmin.class).severe(result.getFailureCause().toString());
		return result;
	}
}
