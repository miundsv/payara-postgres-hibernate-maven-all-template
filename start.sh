#! /bin/bash

set -e

# Getting hold of the database IP
IP=$(docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' jee_db)

# Building the executable jar
mvn clean install
#mvn clean install -pl !server
# build performance can be improved substantially if the jee-web-server is not rebuild every time (see above line).

# Executing the program
java -jar server/target/server.jar $IP app/target/app.war

echo
echo "-------------------------"
echo "The database IP is ${IP}"
echo "-------------------------"
