this maven server project contains two modules:

app : contains the model including persistence logic, as well as the web components for presentation and web service access

server : the embedded payara application server that runs the application

To get started:

* Install latest versions of jdk 8, docker, and maven on your system.
* Start the database by opening a console and executing the script startDb.sh (For linux, otherwise adjust to fit your environment)
* Start the Java program by opening another console, executing the script start.sh

Both scripts start a process each in the foreground, *not* in the background or as a daemon. That means you have to keep the consoles open for as long as you wish the database and/or java server to be accessible. You must therefore open a third console to run the following curl commands.

To test basic web services, enter the following on the command line (assuming curl to be installed)

curl http://localhost:10000/app/hello?name=Felix

To test the persistence web service, enter the following on the command line (assuming curl to be installed):

curl -X PUT http://localhost:10000/app/watermelon/12

To test the servlet enter (in your browser):

http://localhost:10000/app/testForward

To test the jsp enter (in your browser):

http://localhost:10000/app/test

To stop the server, simply quit the running process from the command line, by pressing CTRL + C.
