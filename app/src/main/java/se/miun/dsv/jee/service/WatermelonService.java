package se.miun.dsv.jee.service;

import javax.ejb.Local;

import se.miun.dsv.jee.model.Watermelon;

@Local
public interface WatermelonService {

	Watermelon harvest(int seeds);

}