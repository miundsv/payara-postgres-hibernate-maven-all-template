package se.miun.dsv.jee.service;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import se.miun.dsv.jee.model.Watermelon;

@Stateless
@Local(WatermelonService.class)
public class WatermelonServiceBean implements WatermelonService {

	@PersistenceContext(name = "example")
	EntityManager manager;

	@Override
	public Watermelon harvest(int seeds) {
		Watermelon melon = new Watermelon(seeds);
		manager.persist(melon);
		return melon;
	}
}