package se.miun.dsv.jee.web;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import se.miun.dsv.jee.model.Watermelon;
import se.miun.dsv.jee.service.WatermelonService;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@ApplicationScoped
@Path("/")
public class WebFacade {

	@Inject
	private WatermelonService service;

	@GET
	@Path("hello/")
	public Hello getAddressesFor(@QueryParam("name") String name) {
		return new Hello(name);
	}

	@PUT
	@Path("watermelon/{seeds}")
	public Response initDatabase(@PathParam("seeds") int seeds) {
		if (seeds > 999)
			return Response.ok("OK, so who is going to buy a melon with thousands of seeds?").build();

		Watermelon melon = service.harvest(seeds);

		return Response.accepted().entity(melon).build();
	}
}